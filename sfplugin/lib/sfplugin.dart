import 'dart:async';

import 'package:flutter/services.dart';

class Sfplugin {
  static const MethodChannel _channel =
      const MethodChannel('sfplugin');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
